fetch('https://jsonplaceholder.typicode.com/todos')
    .then((response) => response.json())
    .then((json) => {json.map((item) => {console.log(item.title)})
});

fetch('https://jsonplaceholder.typicode.com/todos/3', {
    method: 'GET'
})
.then((response) => response.json())
.then((json) => console.log(`
    Title: "${json.title}" Status: "${json.completed}"
    `));

fetch('https://jsonplaceholder.typicode.com/todos', {

        method: 'POST',

        header: {
            'Content-Type' : 'application/json'
        },
       
        body: JSON.stringify({
            title: 'hello',
            body: 'Do this dDo that',
            userID: 201,
            completed: false
        }),
    })
    .then((response) => response.json())
    .then((json) => console.log(json));

    fetch('https://jsonplaceholder.typicode.com/todos/1', {

        method: 'PUT',

        header: {
            'Content-Type' : 'application/json'
        },
       
        body: JSON.stringify({
            title: 'hello',
            description: 'Do this dDo that',
            status: 'completed',
            date_completed: 03/03/03,
            userID: 1
        }),
    })
    .then((response) => response.json())
    .then((json) => console.log(json));

    fetch('https://jsonplaceholder.typicode.com/todos/5', {

        method: 'Patch',

        header: {
            'Content-Type' : 'application/json'
        },
       
        body: JSON.stringify({
            title: 'hey'
        }),
    })
    .then((response) => response.json())
    .then((json) => console.log(json));

    fetch('https://jsonplaceholder.typicode.com/posts/201', {
        method: 'DELETE'
    });